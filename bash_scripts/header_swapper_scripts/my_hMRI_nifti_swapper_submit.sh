#!/bin/bash

# courtesy of Nicole Eichert (code and documentation, 2018). Big thanks to Nicole! :D

# Before use:
# - copy this script to a location you prefer
# - edit the path and loop below
#   the looping variable here is subject ID, but it can also be scan name (e.g. W3T_2018_106_004).
# - edit the paths in the script my_hMRI_wrapper.sh

# log in to jalapeno as normal (using ssh -Y yourusername@jalapeno.fmrib.ox.ac.uk).
# call this function from the jalapeno terminal by typing:
# bash /vols/Scratch/neichert/myCode/LarynxCode/my_hMRI_wrapper_submit.sh

scanList="20201210_185653_AlbertoProject_Test9_1_9"


# W3T_2018_106_134

# "W3T_2018_106_006
# W3T_2018_106_008
# W3T_2018_106_007 
# W3T_2018_106_009
# W3T_2018_106_010
#
# W3T_2018_106_012 
# W3T_2018_106_014
# W3T_2018_106_013
# W3T_2018_106_015"

# W3T_2018_106_038
# W3T_2018_106_039
# W3T_2018_106_040
# W3T_2018_106_043
# W3T_2018_106_041
# W3T_2018_106_044
# W3T_2018_106_042
# W3T_2018_106_045




# "W3T_2018_106_038
# W3T_2018_106_039
# W3T_2018_106_040
# W3T_2018_106_043
# W3T_2018_106_041
# W3T_2018_106_044
# W3T_2018_106_042
# W3T_2018_106_045
# W3T_2018_106_046
# W3T_2018_106_049
# W3T_2018_106_047
# W3T_2018_106_052
# W3T_2018_106_054
# W3T_2018_106_058
# W3T_2018_106_064
# W3T_2018_106_066
# W3T_2018_106_068
# W3T_2018_106_069
# W3T_2018_106_074
# W3T_2018_106_076
# W3T_2018_106_079
# W3T_2018_106_082
# W3T_2018_106_087
# W3T_2018_106_088
# W3T_2018_106_089
# W3T_2018_106_091
# W3T_2018_106_096
# W3T_2018_106_098
# W3T_2018_106_102
# W3T_2018_106_104
# W3T_2018_106_108
# W3T_2018_106_109
# W3T_2018_106_114
# W3T_2018_106_120
# W3T_2018_106_123
# W3T_2018_106_121
# W3T_2018_106_129
#
#
#
# W3T_2018_106_002
# W3T_2018_106_003
# W3T_2018_106_006
# W3T_2018_106_008
# W3T_2018_106_007 
# W3T_2018_106_009
# W3T_2018_106_010
#
# W3T_2018_106_012 
# W3T_2018_106_014
# W3T_2018_106_013
# W3T_2018_106_015
# W3T_2018_106_016
# W3T_2018_106_018
# W3T_2018_106_017
# W3T_2018_106_019
# W3T_2018_106_020
# W3T_2018_106_021
# W3T_2018_106_023a
# W3T_2018_106_024
# W3T_2018_106_025
# W3T_2018_106_026
# W3T_2018_106_028
# W3T_2018_106_029
# W3T_2018_106_030
# W3T_2018_106_032
# W3T_2018_106_031
# W3T_2018_106_033
# W3T_2018_106_035
# W3T_2018_106_036
# W3T_2018_106_118
# W3T_2018_106_117
# W3T_2018_106_119
# W3T_2018_106_134
# W3T_2018_106_135
# W3T_2018_106_132
# "



module add MATLAB
module add spm/12_R2017b

# cd ../../Data/MRdata/alazari

for scan in $scanList; do
  echo "$scan"
  # cd $scan
  # rm -r fieldmap_gre_2mm_208mm_6_1/
  # rm -r fieldmap_gre_2mm_208mm_7_1/
  # rm -r hMRI_NIFTI/
  # rm -r RFCorr_results/
  # cd ..
  #
  # cd ../../../Scratch/alazari/
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_MT.sh $scan
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_T1.sh $scan
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_nifti_swapper_PD.sh $scan
  # cd ../../Data/MRdata/alazari
done
