#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/myelin/Mohamed
scriptDir=/vols/Scratch/alazari/preclinicalMPM_scripts/

subjList="20201210_185653_AlbertoProject_Test9_1_9"

for subj in $subjList; do

  rm $scriptDir/acquisition_order_"$subj".txt

  echo "identifying scans for $i"

  for a in {1..90}; do

  echo "$a"

  # $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$a"/acqp
  # grep -w "ACQ_protocol_name=( 64 )" $workDir/"$subj"_Merima_Doldcr_T2w_Diffusion/"$a"/acqp
  name=$(sed -n '13p' $workDir/"$subj"/"$a"/acqp)
  echo "$a" "$name" >> $scriptDir/acquisition_order_"$subj".txt

  done

echo "done"

done
