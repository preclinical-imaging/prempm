#!/bin/bash

# bash-Matlab wrapper interface is courtesy of Nicole Eichert (code and documentation, 2018). Big thanks to Nicole! :D

# Before use:
# - copy this script to a location you prefer
# - edit the path and loop below
#   the looping variable here is subject ID, but it can also be scan name (e.g. W3T_2018_106_004).
# - edit the paths in the script my_hMRI_wrapper.sh

# log in to jalapeno as normal (using ssh -Y yourusername@jalapeno.fmrib.ox.ac.uk).
# call this function from the jalapeno terminal by typing:
# bash /vols/Scratch/neichert/myCode/LarynxCode/my_hMRI_wrapper_submit.sh

scanList="20210602_161923_AL17_AL17_MPM_2_1_4"


module add MATLAB
module add spm/12_R2017b

for scan in $scanList; do
  echo "$scan"

  # cd ../../../Scratch/alazari/
  fsl_sub -q long.q -N hMRI bash /vols/Scratch/alazari/preclinicalMPM_scripts/MPM_pipeline/my_hMRI_wrapper.sh $scan
  # cd ../../Data/MRdata/alazari
done
