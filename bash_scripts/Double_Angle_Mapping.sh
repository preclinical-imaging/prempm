#!/usr/bin/env bash

# definitions
subjList="20210601_194649_AL12_MPM_2_1_3"

scanList="
80@79
"
# 40@80

for subj in $subjList; do

  workDir=/vols/Scratch/premotor/MPM_rodent_data/"$subj"_NIFTI
  mkdir -p $workDir/DAM

for i in $scanList; do

  # separate the two scans
  Angle1=${i%@*} #with flip angle alpha
  Angle2=${i#*@} #with flip angle 2*alpha
  echo $Angle1
  echo $Angle2

  # separate the two scans
  S1=$workDir/MGE2D_DAM_TR75s_250iso_FA40_"$Angle1"0001/*.nii #with flip angle alpha
  S2=$workDir/MGE2D_DAM_TR75s_250iso_FA80_"$Angle2"0001/*.nii #with flip angle 2*alpha

  echo $S1
  echo $S2
  echo "performing DAM"

  fslmaths $S1 -mul 2 $workDir/DAM/doubleS1
  fslmaths $S2 -div $workDir/DAM/doubleS1 $workDir/DAM/S2_divided_by_doubleS1
  fslmaths $workDir/DAM/S2_divided_by_doubleS1 -acos $workDir/DAM/Alpha_from_arccosine_S2_S1
  fslmaths $workDir/DAM/S2_divided_by_doubleS1 -mul 180 -div 3.141592 -div 40 -mul 100 $workDir/DAM/B1map_DAM
  cp $S1 $workDir/DAM/B1map_struct.nii
  cp $workDir/MGE2D_DAM_TR75s_250iso_FA40_"$Angle1"0001/*.json $workDir/DAM/B1map_struct.json

done

echo "done"

done
