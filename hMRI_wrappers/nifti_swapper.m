%
function nifti_swapper(olddir,newdir)
nifti_old=dir(strcat(olddir,filesep,'*.nii'));
tf2=false(size(nifti_old,1),1);
for k=1:size(nifti_old)
    if strfind(nifti_old(k).name(1:2),'._')==1
        tf2(k)=true;
    end
end
nifti_old(tf2)=[];

nifti_new=dir(strcat(newdir,filesep,'*.nii'));
tf2=false(size(nifti_new,1),1);
for k=1:size(nifti_new)
    if strfind(nifti_new(k).name(1:2),'._')==1
        tf2(k)=true;
    end
end
nifti_new(tf2)=[];

% read in files
for i=1:size(nifti_old,1)
old_files_spm(i)=spm_vol(strcat(nifti_old(i).folder,filesep,nifti_old(i).name));
end
for i=1:size(nifti_old,1)
new_files_spm(i)=spm_vol(strcat(nifti_new(i).folder,filesep,nifti_new(i).name));
end
% copy over descp field
for i=1:size(new_files_spm,2)
    new_files_spm(i).descrip = old_files_spm(i).descrip;
end
% save modified nifties
for i=1:size(new_files_spm,2)
    spm_create_vol(new_files_spm(i));
end
disp('Nifit headers files corrected')
    
