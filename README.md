# preMPM 

## Preprocessing weighted scans for preclinical Multi-Parameter Mapping

![echo_averaging_2](images/mouse_MPM_pilot.png)

This pipeline preprocesses and averages echoes from multiple sequence repetitions to enhance the overall SNR of the quantitative maps. 
![echo_averaging_2](images/echo_averaging_2.png)

It also includes functions to preprocess B1+ mapping and turn the scans into hMRI-compatible format.
![echo_averaging_2](images/hMRI_inputs.png)

