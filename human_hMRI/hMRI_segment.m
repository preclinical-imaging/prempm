function hMRI_segment(directory)

dirpath=dir(directory);
for i=1:size(dirpath)
    if strfind(dirpath(i).name, 'RFCorr')>0
        RFcorrindex=i;
    end
end
RFdirectory=dirpath(RFcorrindex);
RFCorr_files=dir(strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results'));

for i=1:size(RFCorr_files,1)
    if strfind(RFCorr_files(i).name,'MT.nii')>0
        MTfileindex=i;
    end
    if strfind(RFCorr_files(i).name,'PD.nii')>0
        PDfileindex=i;
    end
    if strfind(RFCorr_files(i).name,'R2s_OLS.nii')>0
        R2sfileindex=i;
    end
    if strfind(RFCorr_files(i).name,'R1.nii')>0
        R1fileindex=i;
    end
end
MTfile=strcat(RFCorr_files(MTfileindex).folder,filesep,RFCorr_files(MTfileindex).name);
R1file=strcat(RFCorr_files(R1fileindex).folder,filesep,RFCorr_files(R1fileindex).name);
R2sfile=strcat(RFCorr_files(R2sfileindex).folder,filesep,RFCorr_files(R2sfileindex).name);
PDfile=strcat(RFCorr_files(PDfileindex).folder,filesep,RFCorr_files(PDfileindex).name);


spm('defaults','fmri');
spm_jobman('initcfg');
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.output.indir = 1;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel.vols = {MTfile};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel.biasreg = 0;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel.biasfwhm = Inf;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.channel.write = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vols_pm = {};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.vox = [1 1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.many_sdatas.bb = [-78 -112 -70
                                                                        78 76 85];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,1'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).ngaus = 2;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).native = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).warped = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,2'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).ngaus = 2;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).native = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).warped = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,3'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).ngaus = 2;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).native = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).warped = [1 1];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,4'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).ngaus = 3;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).native = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).warped = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,5'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).ngaus = 4;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).native = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).warped = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).tpm = {'/opt/fmrib/hMRI-Toolbox/etpm/eTPM.nii,6'};
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).ngaus = 2;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).native = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).warped = [0 0];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.mrf = 1;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.cleanup = 1;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.affreg = 'mni';
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.fwhm = 0;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.samp = 3;
matlabbatch{1}.spm.tools.hmri.proc.proc_modul.proc_us.warp.write = [0 1];
spm_jobman('run',matlabbatch);
clear matlabbatch


% fish out tissue parameter files
RFCorr_files=dir(strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results'));

for i=1:size(RFCorr_files,1)
    if (strfind(RFCorr_files(i).name,'c1')>0) & (strfind(RFCorr_files(i).name,'c1')<2)
        GMmaskindex=i;
    end
    if strfind(RFCorr_files(i).name,'c2')>0 & strfind(RFCorr_files(i).name,'c2')<2
        WMmaskindex=i;
    end
    if strfind(RFCorr_files(i).name,'c3')>0 & strfind(RFCorr_files(i).name,'c3')<2
        CSFmaskindex=i;
    end
end
GMfile=strcat(RFCorr_files(GMmaskindex).folder,filesep,RFCorr_files(GMmaskindex).name);
WMfile=strcat(RFCorr_files(WMmaskindex).folder,filesep,RFCorr_files(WMmaskindex).name);
CSFfile=strcat(RFCorr_files(CSFmaskindex).folder,filesep,RFCorr_files(CSFmaskindex).name);
tissuefiles={GMfile,WMfile,CSFfile}';
maskname=strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results',filesep,'brainmask.nii');
% combine all of these
matlabbatch{1}.spm.util.imcalc.input = tissuefiles;
matlabbatch{1}.spm.util.imcalc.output = 'brainmask';
matlabbatch{1}.spm.util.imcalc.outdir = {strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results')};
matlabbatch{1}.spm.util.imcalc.expression = '((i1>0)+(i2>0)+(i3>0))>0';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch

%apply to other files
matlabbatch{1}.spm.util.imcalc.input = {maskname,R1file}';
matlabbatch{1}.spm.util.imcalc.output = 'R1_masked';
matlabbatch{1}.spm.util.imcalc.outdir = {strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results')};
matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch

matlabbatch{1}.spm.util.imcalc.input = {maskname,PDfile}';
matlabbatch{1}.spm.util.imcalc.output = 'PD_masked';
matlabbatch{1}.spm.util.imcalc.outdir = {strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results')};
matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch

matlabbatch{1}.spm.util.imcalc.input = {maskname,MTfile}';
matlabbatch{1}.spm.util.imcalc.output = 'MT_masked';
matlabbatch{1}.spm.util.imcalc.outdir = {strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results')};
matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch

matlabbatch{1}.spm.util.imcalc.input = {maskname,R2sfile}';
matlabbatch{1}.spm.util.imcalc.output = 'R2s_masked';
matlabbatch{1}.spm.util.imcalc.outdir = {strcat(RFdirectory.folder,filesep,RFdirectory.name,filesep,'Results')};
matlabbatch{1}.spm.util.imcalc.expression = 'i1.*i2';
matlabbatch{1}.spm.util.imcalc.var = struct('name', {}, 'value', {});
matlabbatch{1}.spm.util.imcalc.options.dmtx = 0;
matlabbatch{1}.spm.util.imcalc.options.mask = 0;
matlabbatch{1}.spm.util.imcalc.options.interp = 1;
matlabbatch{1}.spm.util.imcalc.options.dtype = 4;
spm_jobman('run',matlabbatch);
clear matlabbatch